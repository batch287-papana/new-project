const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const app = express();

const userRoutes = require("./routes/userRoute.js");
const restaurantRoutes = require("./routes/restaurantRoute.js");
const itemRoutes = require("./routes/itemRoute.js");


mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.lzuuwol.mongodb.net/Food-delivery",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

mongoose.connection.once("open", ()=> console.log("Now connected in the cloud."));

app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());
app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${process.env.PORT || 4000}`)
});
app.use(express.static("restaurant-images"));
app.use(express.static("item-images"));
app.use("/users", userRoutes);
app.use("/restaurants", restaurantRoutes);
app.use("/items", itemRoutes);

