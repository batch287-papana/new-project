const User = require("../models/User");
const Restaurant = require("../models/Restaurant");
const Item = require("../models/Item");
const bcrypt = require("bcrypt");
const auth = require("../auth");



module.exports.checkEmailExists = (reqBody) => {
	
	return User.find({ email: reqBody.email }).then(result => {
		if(result.length > 0){
			
			return true
		} else {
			
			return false
		
		}
	})
}

module.exports.registerUser = (reqBody) => {

	console.log(reqBody);

	let newUser = new User({
		name: reqBody.name,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		address: reqBody.address,
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save()
		.then(user => {
			// Successfully saved user
			return true;
		})
		.catch(error => {
			// Error while saving user
			console.error(error);
			return false;
		});
}



module.exports.loginUser = (reqBody) => {

	return User.findOne({ email: reqBody.email }).then(result => {

		console.log(result);

		if(result == null){

			return false;

		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			console.log(result);
			if(isPasswordCorrect){

				let accessData = { access: auth.createAccessToken(result) };
				console.log(accessData);

				if (result.isAdmin) {
					accessData.isAdmin = true; // Set isAdmin to true if the user is an admin
				}

				return accessData;

			} else {

				return false;

			}

		}
	});
};



module.exports.getProfile = (data) => {
  return User.findOne({ _id: data.userId }).then(result => {
    if (result == null) {
      return false;
    } else {
      result.password = "";
      return result;
    }
  });
};


module.exports.add_to_cart = async (data) => {
  try {
    const { userId, itemId, restaurantId } = data;

    // Find the user by userId
    const user = await User.findById(userId);

    if (!user) {
      return { success: false, message: "User not found" };
    }

    // Find the item by itemId
    const item = await Item.findById(itemId);

    if (!item) {
      return { success: false, message: "Item not found" };
    }

    // Check if the item already exists in the user's cart for the same restaurant
    const existingCartItemIndex = user.cart.findIndex(
      (cartItem) =>
        cartItem.itemId === itemId && cartItem.restaurantId === restaurantId
    );

    if (existingCartItemIndex !== -1) {
      // Update the quantity and subtotal of the existing item in the cart
      user.cart[existingCartItemIndex].quantity += 1;
      user.cart[existingCartItemIndex].subTotal += item.price;
    } else {
      // Add a new entry to the cart for items from different restaurants
      user.cart.push({
        itemId,
        restaurantId,
        quantity: 1,
        subTotal: item.price,
      });
    }

    // Save the updated user with the new cart data
    await user.save();

    return { success: true, message: "Item added to cart successfully" };
  } catch (error) {
    console.error(error);
    return { success: false, message: "An error occurred while adding to cart" };
  }
};



module.exports.remove_from_cart = async (data) => {
  try {
    const { userId, itemId, restaurantId } = data;

    // Find the user by userId
    const user = await User.findById(userId);

    if (!user) {
      return { success: false, message: "User not found" };
    }

    // Find the index of the item in the user's cart based on itemId and restaurantId
    const cartItemIndex = user.cart.findIndex(
      (cartItem) =>
        cartItem.itemId === itemId && cartItem.restaurantId === restaurantId
    );

    if (cartItemIndex !== -1) {
      // Remove the item from the cart
      user.cart.splice(cartItemIndex, 1);

      // Save the updated user with the item removed from the cart
      await user.save();

      return { success: true, message: "Item removed from cart successfully" };
    } else {
      return { success: false, message: "Item not found in cart" };
    }
  } catch (error) {
    console.error(error);
    return { success: false, message: "An error occurred while removing from cart" };
  }
};


module.exports.view_cart = async (userId) => {
  try {
    // Find the user by userId
    const user = await User.findById(userId);

    if (!user) {
      return { success: false, message: "User not found" };
    }

    // Return the user's cart
    return { success: true, cart: user.cart };
  } catch (error) {
    console.error(error);
    return { success: false, message: "An error occurred while fetching the cart" };
  }
};


module.exports.view_favorites = async (userId) => {
  try {
    // Find the user by userId
    const user = await User.findById(userId);

    if (!user) {
      return { success: false, message: "User not found" };
    }

    // Return the user's favorite restaurants array
    return { success: true, favorites: user.favoriteRestaurants };
  } catch (error) {
    console.error(error);
    return { success: false, message: "An error occurred while fetching favorite restaurants" };
  }
};



module.exports.make_favorite = async (data) => {
  try {
    const { userId, restaurantId } = data;

    // Find the user by userId
    const user = await User.findById(userId);

    if (!user) {
      return { success: false, message: "User not found" };
    }

    // Check if the restaurantId already exists in the user's favoriteRestaurants
    if (user.favoriteRestaurants.includes(restaurantId)) {
      return { success: false, message: "Restaurant is already in favorites" };
    }

    // Add the restaurantId to the user's favoriteRestaurants array
    user.favoriteRestaurants.push(restaurantId);

    // Save the updated user with the new favorite restaurant
    await user.save();

    return { success: true, message: "Restaurant added to favorites successfully" };
  } catch (error) {
    console.error(error);
    return { success: false, message: "An error occurred while adding to favorites" };
  }
};


module.exports.remove_favorite = async (data) => {
  try {
    const { userId, restaurantId } = data;

    // Find the user by userId
    const user = await User.findById(userId);

    if (!user) {
      return { success: false, message: "User not found" };
    }

    // Check if the restaurantId exists in the user's favoriteRestaurants
    const restaurantIndex = user.favoriteRestaurants.indexOf(restaurantId);

    if (restaurantIndex === -1) {
      return { success: false, message: "Restaurant is not in favorites" };
    }

    // Remove the restaurantId from the user's favoriteRestaurants array
    user.favoriteRestaurants.splice(restaurantIndex, 1);

    // Save the updated user with the removed favorite restaurant
    await user.save();

    return { success: true, message: "Restaurant removed from favorites successfully" };
  } catch (error) {
    console.error(error);
    return { success: false, message: "An error occurred while removing from favorites" };
  }
};


// UserController.js

// ...

module.exports.update_name = async (userId, newName) => {
  try {
    const user = await User.findById(userId);

    if (!user) {
      return { success: false, message: "User not found" };
    }

    user.name = newName;
    await user.save();

    return { success: true, message: "Name updated successfully" };
  } catch (error) {
    console.error(error);
    return { success: false, message: "An error occurred while updating name" };
  }
};

module.exports.update_number = async (userId, newNumber) => {
  try {
    const user = await User.findById(userId);

    if (!user) {
      return { success: false, message: "User not found" };
    }

    user.mobileNo = newNumber;
    await user.save();

    return { success: true, message: "Mobile number updated successfully" };
  } catch (error) {
    console.error(error);
    return { success: false, message: "An error occurred while updating mobile number" };
  }
};

module.exports.update_password = async (userId, newPassword) => {
  try {
    const user = await User.findById(userId);

    if (!user) {
      return { success: false, message: "User not found" };
    }

    user.password = bcrypt.hashSync(newPassword, 10);
    await user.save();

    return { success: true, message: "Password updated successfully" };
  } catch (error) {
    console.error(error);
    return { success: false, message: "An error occurred while updating password" };
  }
};

module.exports.update_address = async (userId, newAddress) => {
  try {
    const user = await User.findById(userId);

    if (!user) {
      return { success: false, message: "User not found" };
    }

    user.address = newAddress;
    await user.save();

    return { success: true, message: "Address updated successfully" };
  } catch (error) {
    console.error(error);
    return { success: false, message: "An error occurred while updating address" };
  }
};


module.exports.order = async (userId) => {
  try {
    // Find the user by userId
    const user = await User.findById(userId);

    if (!user) {
      return { success: false, message: "User not found" };
    }

    // Clear the user's cart by setting it to an empty array
    user.cart = [];

    // Save the updated user
    await user.save();

    // Perform additional order processing steps here (e.g., send order confirmation, charge payment, etc.)

    return { success: true, message: "Order placed successfully" };
  } catch (error) {
    console.error(error);
    return { success: false, message: "An error occurred while placing the order" };
  }
};


