const Restaurant = require("../models/Restaurant");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.createRestaurant = async (restaurantData) => {
  try {
    // Extract the imageUrl from restaurantData
    const { name, rating, menu, tags, imageUrl } = req.body;

  const newRestaurant = new Restaurant({
    name,
    rating,
    menu,
    tags,
    imageUrl, // Store the image URL
  });

    await restaurant.save();
    return { success: true, message: "Restaurant created successfully" };
  } catch (error) {
    console.error(error);
    return { success: false, message: "Error creating restaurant" };
  }
};

module.exports.editRestaurant = async (restaurantId, updatedData) => {
  try {
    const restaurant = await Restaurant.findByIdAndUpdate(restaurantId, updatedData, { new: true });
    if (!restaurant) {
      return { success: false, message: "Restaurant not found" };
    }
    return { success: true, message: "Restaurant edited successfully" };
  } catch (error) {
    console.error(error);
    return { success: false, message: "An error occurred while editing the restaurant" };
  }
};

module.exports.viewAllRestaurants = async () => {
  try {
    const restaurants = await Restaurant.find();
    return { success: true, restaurants };
  } catch (error) {
    console.error(error);
    return { success: false, message: "Error fetching restaurants" };
  }
};

module.exports.viewRestaurantsByTag = async (tag) => {
  try {
    const restaurants = await Restaurant.find({ "tags.tagName": tag });
    return { success: true, restaurants };
  } catch (error) {
    console.error(error);
    return { success: false, message: "Error fetching restaurants by tag" };
  }
};

module.exports.viewRestaurant = async (restaurantId) => {
  try {
    const restaurant = await Restaurant.findById(restaurantId);
    if (restaurant) {
      return { success: true,  restaurant };
    } else {
      return { success: false, message: "Restaurant not found" };
    }
  } catch (error) {
    console.error(error);
    return { success: false, message: "Error fetching restaurant menu" };
  }
};
