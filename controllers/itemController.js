const Item = require("../models/Item");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.createItem = async (itemData) => {
  try {
    const item = new Item(itemData);
    await item.save();
    return { success: true, message: "Item created successfully" };
  } catch (error) {
    console.error(error);
    return { success: false, message: "Error creating item" };
  }
};

module.exports.editItem = async (itemId, updatedData) => {
  try {
    const item = await Item.findByIdAndUpdate(itemId, updatedData, { new: true });
    if (!item) {
      return { success: false, message: "Item not found" };
    }
    return { success: true, message: "Item edited successfully" };
  } catch (error) {
    console.error(error);
    return { success: false, message: "An error occurred while editing the item" };
  }
};
