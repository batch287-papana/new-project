const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");

router.post("/login", (req,res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/register", (req,res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/checkEmail", (req,res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

router.get("/details", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  console.log(userData);
  userController.getProfile({ userId: userData.id }).then(resultFromController => res.send(resultFromController));
});

router.post("/order", auth.verify, async (req, res) => {
  const userId = auth.decode(req.headers.authorization).id;

  const result = await userController.order(userId);
  res.send(result);
});

router.post("/add_to_cart", auth.verify, (req, res) => {

	let data = {
		userId: auth.decode(req.headers.authorization).id,
		itemId: req.body.itemId,
		restaurantId: req.body.restaurantId
	};

	userController.add_to_cart(data).then(resultFromController => res.send(resultFromController));
});

router.post("/remove_from_cart", auth.verify, (req, res) => {

	let data = {
		userId: auth.decode(req.headers.authorization).id,
		itemId: req.body.itemId,
		restaurantId: req.body.restaurantId
	};

	userController.remove_from_cart(data).then(resultFromController => res.send(resultFromController));
});

router.post("/view_cart", auth.verify, (req, res) => {

	const  userId = auth.decode(req.headers.authorization).id;

	userController.add_to_cart(userId).then(resultFromController => res.send(resultFromController));
});

router.post("/make_favorite", auth.verify, (req, res) => {

	let data = {
		userId: auth.decode(req.headers.authorization).id,
		restaurantId: req.body.restaurantId
	};

	userController.make_favorite(data).then(resultFromController => res.send(resultFromController));
});

router.post("/remove_favorite", auth.verify, (req, res) => {

	let data = {
		userId: auth.decode(req.headers.authorization).id,
		restaurantId: req.body.restaurantId
	};

	userController.remove_favorite(data).then(resultFromController => res.send(resultFromController));
});

router.post("/view_favorites", auth.verify, (req, res) => {

	const  userId = auth.decode(req.headers.authorization).id;

	userController.view_favorites(userId).then(resultFromController => res.send(resultFromController));
});

router.post("/update_name", auth.verify, async (req, res) => {
  const userId = auth.decode(req.headers.authorization).id;
  const { newName } = req.body;

  const result = await userController.update_name(userId, newName);
  res.send(result);
});

// Route to update user's mobile number
router.post("/update_number", auth.verify, async (req, res) => {
  const userId = auth.decode(req.headers.authorization).id;
  const { newNumber } = req.body;

  const result = await userController.update_number(userId, newNumber);
  res.send(result);
});

// Route to update user's password
router.post("/update_password", auth.verify, async (req, res) => {
  const userId = auth.decode(req.headers.authorization).id;
  const { newPassword } = req.body;

  const result = await userController.update_password(userId, newPassword);
  res.send(result);
});

// Route to update user's address
router.post("/update_address", auth.verify, async (req, res) => {
  const userId = auth.decode(req.headers.authorization).id;
  const { newAddress } = req.body;

  const result = await userController.update_address(userId, newAddress);
  res.send(result);
});

module.exports = router;