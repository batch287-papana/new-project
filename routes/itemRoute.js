const express = require("express");
const router = express.Router();
const itemController = require("../controllers/itemController");
const auth = require("../auth");

// Create a new item (only for admin)
router.post("/create_item", auth.verifyAdmin, (req, res) => {
  itemController.createItem(req.body).then(result => res.send(result));
});

// Edit an item (only for admin)
router.post("/edit_item", auth.verifyAdmin, (req, res) => {
  const { itemId } = req.body;
  itemController.editItem(itemId, req.body).then(result => res.send(result));
});

module.exports = router;
