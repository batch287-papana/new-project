const express = require("express");
const router = express.Router();
const restaurantController = require("../controllers/restaurantController");
const auth = require("../auth");

// Create a new restaurant (only for admin)
router.post("/create_restaurant", auth.verifyAdmin, (req, res) => {
  restaurantController.createRestaurant(req.body).then(result => res.send(result));
});

// Edit a restaurant (only for admin)
router.post("/edit_restaurant", auth.verifyAdmin, (req, res) => {
  const { restaurantId } = req.body;
  restaurantController.editRestaurant(restaurantId, req.body).then(result => res.send(result));
});


// View all restaurants
router.get("/view_all", (req, res) => {
  restaurantController.viewAllRestaurants().then(result => res.send(result));
});

// View restaurants by tag
router.post("/view_tag", (req, res) => {
  const { tag } = req.body;
  restaurantController.viewRestaurantsByTag(tag).then(result => res.send(result));
});

// View a specific restaurant's menu by restaurant ID
router.get("/view_restaurant", (req, res) => {
  const { restaurantId } = req.body;
  restaurantController.viewRestaurant(restaurantId).then(result => res.send(result));
});

module.exports = router;
