const mongoose = require("mongoose");

const restaurantSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Name of restaurant is required."]
	},
	rating: {
		type: Number,
		required: [true, "Rating is required."]
	},
	imageUrl: {
		type: String,
		//required: true
	},
	menu: [
		{
			itemId: {
				type: String,
				required: [ true, "Item Id is required."]
			}
		}
	],
	tags: [
		{
			tagName: {
				type: String,
				required: [true, "Tag Name is required."]
			}
		}
	]

})
module.exports = mongoose.model("Restaurant", restaurantSchema);