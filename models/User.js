const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	name:{
		type: String,
		required: [true, "Name is required."]
	},
	email: {
		type: String,
		required: [ true, "Email is required." ]
	},
	password: {
		type: String,
		required: [ true, "Password is required." ]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	mobileNo: {
		type: String,
		required: [ true, "Mobile number is required." ]
	},
	address: {
		type: String,
		required: [ true, "Address is required." ]
	},
	cart : [
		{
			restaurantId: {
				type: String,
				required: [ true, "Restaurant Id is required."]
			},
			itemId: {
				type: String,
				required: [ true, "Item Id is required."]
			},
			quantity: {
				type: Number,
				required: true
			},
			subTotal: {
				type: Number,
				required: true
			}

		}
	],
	favoriteRestaurants: [
		{
			restaurantId: {
				type: String,
				required: [ true, "Restaurant Id is required."]
			}
		}
	]

})
module.exports = mongoose.model("User", userSchema);