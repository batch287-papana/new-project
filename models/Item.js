const mongoose = require("mongoose");

const itemSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Name of item is required."]
	},
	description: {
		type: String,
		required: [true, "Description is required."]
	},
	imageUrl: {
		type: String,
		//required: true
	},
	rating: {
		type: String,
		required: [true, "Rating is required."]
	},
	price: {
		type: Number,
		required: [true, "Price is required."]
	}
})
module.exports = mongoose.model("Item", itemSchema);